<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('dump', 'HomeController@index');

//Admin
Route::get('login_page', 'AdminController@login_page');
Route::get('admin/dashboard', 'AdminController@index');
Route::get('admin/posts', 'PostController@index');

//API
Route::get('api/post', 'ApiController@post');
