<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login Admin</title>


    <link href="{{url('')}}/vendor/admin-login/css/style.css" rel="stylesheet">

    <!-- Bootstrap Core CSS -->
    <link href="{{url('')}}/vendor/sb-admin/css/bootstrap.min.css" rel="stylesheet">

</head>
<body>
    <div class="login-page">
        @if(Session::get('alert'))
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{Session::get('alert')}}
        </div>
        @endif
        <div class="form">
            <form class="register-form">
                <input type="text" placeholder="name"/>
                <input type="password" placeholder="password"/>
                <input type="text" placeholder="email address"/>
                <button>create</button>
                <p class="message">Already registered? <a href="#">Sign In</a></p>
            </form>
            <form class="login-form" action="{{url('admin_login_check')}}" method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="text" name="username" placeholder="username"/>
                <input type="password" name="password" placeholder="password"/>
                <button type="submit">login</button>
                <p class="message">Not registered? <a href="#">Create an account</a></p>
            </form>
        </div>
    </div>
</body>
</html>

<script src="{{url('')}}/vendor/admin-login/js/main.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{url('')}}/vendor/sb-admin/js/bootstrap.min.js"></script>
<!-- jQuery -->
<script src="{{url('')}}/vendor/sb-admin/js/jquery.js"></script>
