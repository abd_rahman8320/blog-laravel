<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin Page | Manage Post</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{url('')}}/vendor/sb-admin/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{url('')}}/vendor/sb-admin/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="{{url('')}}/vendor/sb-admin/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{url('')}}/vendor/sb-admin/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- DataTable -->
    <link rel="stylesheet" type="text/css" href="{{url('')}}/vendor/dataTables/css/dataTables.bootstrap.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        @extends('layouts.menu_admin')

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Post <small>Manage</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                Admin
                            </li>
                            <li>
                                Post
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <table id="dataTables" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Judul</th>
                                    <th>Tanggal Input</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbodys>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="{{url('')}}/vendor/sb-admin/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{url('')}}/vendor/sb-admin/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="{{url('')}}/vendor/sb-admin/js/plugins/morris/raphael.min.js"></script>
    <script src="{{url('')}}/vendor/sb-admin/js/plugins/morris/morris.min.js"></script>
    <script src="{{url('')}}/vendor/sb-admin/js/plugins/morris/morris-data.js"></script>

    <!-- DataTable -->
    <script src="{{url('')}}/vendor/dataTables/jquery/jquery-3.3.1.js"></script>
    <script src="{{url('')}}/vendor/dataTables/js/jquery.dataTables.min.js"></script>
    <script src="{{url('')}}/vendor/dataTables/js/dataTables.bootstrap.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function(){
        $('#dataTables').DataTable({
            "ajax": "{{url('')}}/api/post",
            "columns": [
                {"data":"id"},
                {"data":"title"},
                {"data":"created_at"},
                {"data":"action"}
            ]
        });
    });
    // $.ajax({
    //     url: '{{url('')}}/api/post',
    //     type: 'get',
    //     success: function(result){
    //         for (var i = 0; i < result.length; i++) {
    //             $('#data').append('<tr id="'+result[i].id+'"></tr>');
    //             $('#'+result[i].id).append('<td>'+i+'</td><td>'+result[i].title+'</td><td>'+result[i].created_at+'</td>');
    //         }
    //     }
    // });
    </script>
</body>

</html>
