<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Log;
use App\Post;

class HomeController extends Controller
{
    public function index()
    {
        $data = new Post;
        $data->title = 'Judul';
        $data->content = 'Konten';
        $data->posted_by = 'Admin';
        $data->save();

        $data = new Post;
        $data->title = 'Judul2';
        $data->content = 'Konten2';
        $data->posted_by = 'Admin';
        $data->save();

        return "success";
    }
}
