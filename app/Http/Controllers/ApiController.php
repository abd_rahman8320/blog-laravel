<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ApiController extends Controller
{
    public function post()
    {
        $get = DB::table('posts')
        ->select('title', 'created_at')
        ->get();

        $i=1;
        foreach ($get as $row) {
            $row->id = $i;
            $row->action = "<button class='btn btn-info'>Edit</button> <button class='btn btn-danger'>Delete</button>";
            $i++;
        }

        $data = [
            'data' => $get
        ];

        return response()->json($data);
    }
}
