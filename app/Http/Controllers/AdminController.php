<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function login_page()
    {
        return view('admin.login');
    }

    public function index(Request $request)
    {
        return view('admin.dashboard')
        ->with('request', $request);
    }
}
